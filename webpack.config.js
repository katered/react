var webpack = require('webpack');

module.exports = {
	entry: "./src/main.js",
	output: {
		path: __dirname + '/public/build/',
		publicPath: "build/",
		filename: "bundle.js"
	},
	module: {
		rules: [
			{
				test: /\.js$/,
				use: "babel-loader",
				exclude: [/node_modules/, /public/]
			},
			{
				test: /\.css$/,
				use: "style-loader!css-loader!autoprefixer-loader",
				exclude: [/node_modules/, /public/]
			},
            {
                test: /\.jsx$/,
                loader: "react-hot-loader!babel-loader",
                exclude: [/node_modules/, /public/]
            },
			{
				test: /\.json$/,
				use: "json-loader"
			}
		]
	}
}
